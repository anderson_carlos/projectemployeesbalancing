package com.example.anderson.employeesbalancing.features.addteam

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.features.addteam.ViewHolderAddEmployeeContract
import kotlinx.android.synthetic.main.item_add_employee.view.*


/**
 * Created by Anderson on 27/10/2018.
 */
class ViewHolderAddEmployee(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindView(employee: Employee, mViewHolderAddEmployeeContract: ViewHolderAddEmployeeContract) {

        val txtAddTeamName = itemView.add_employee_name
        val txtPLevel = itemView.add_plevel_employee

        txtAddTeamName.text = employee.nameEmployee
        txtPLevel.text = "- ${employee.plevel}"

        itemView.checkbox_add_employee.setOnClickListener {

            if (itemView.checkbox_add_employee.isChecked) {

                mViewHolderAddEmployeeContract.addObjectListEmployees(employee)

            } else {

                mViewHolderAddEmployeeContract.removeObjectListEmployees(employee)

            }
        }

    }

}