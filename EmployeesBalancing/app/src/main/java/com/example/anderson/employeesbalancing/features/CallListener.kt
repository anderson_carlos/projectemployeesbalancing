package com.example.anderson.employeesbalancing.features

import com.example.anderson.employeesbalancing.entity.Employee

/**
 * Created by Anderson on 30/10/2018.
 */
interface CallListener {

    fun objects(any: Any)

}