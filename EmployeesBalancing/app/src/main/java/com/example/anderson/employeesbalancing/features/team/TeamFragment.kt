package com.example.anderson.employeesbalancing.features.team

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.entity.Team
import com.example.anderson.employeesbalancing.features.CallListener
import kotlinx.android.synthetic.main.fragment_team.*


class TeamFragment : Fragment(), TeamContract.TeamView {

    private lateinit var teamPresenter: TeamPresenter
    private lateinit var mCallListener: CallListener
    private lateinit var textResultBalance: TextView


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_team, container, false)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewTeam.layoutManager = LinearLayoutManager(activity)

        textResultBalance = result_balance

        teamPresenter = TeamPresenter(this)


    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallListener = context as CallListener
    }


    override fun callBackGetListTeams(teams: ArrayList<Team>) {

        recyclerViewTeam.adapter = AdapterRecyclerViewListTeam(teams, mCallListener)
    }

    override fun onResume() {
        super.onResume()

        teamPresenter.getListTeams()
        teamPresenter.caulateResultBalance()

    }

    override fun callBackResultBlance(result: Int) {
        textResultBalance.text = result.toString()
        if (result != 0) {

            when {
                result <= 2 -> Toast.makeText(activity, getString(R.string.msg_good_balance), Toast.LENGTH_SHORT).show()
                result in 3..4 -> Toast.makeText(activity, getString(R.string.msg_more_less_balance), Toast.LENGTH_SHORT).show()
                result > 4 -> Toast.makeText(activity, getString(R.string.msg_not_good_balance), Toast.LENGTH_SHORT).show()
            }


        }


    }


}
