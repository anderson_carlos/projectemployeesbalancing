package com.example.anderson.employeesbalancing

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.MenuItem
import com.example.anderson.employeesbalancing.entity.Team
import com.example.anderson.employeesbalancing.features.CallListener
import com.example.anderson.employeesbalancing.features.addteam.AddEmployeeActivity
import com.example.anderson.employeesbalancing.features.employee.EmployeeFragment
import com.example.anderson.employeesbalancing.features.loadCsv.LoadCsvFragment
import com.example.anderson.employeesbalancing.features.team.TeamFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener, CallListener {

    lateinit var selectedFragment: Fragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        navigation_bottom.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            R.id.navigation_load -> {

                selectedFragment = LoadCsvFragment()

                openFragment(selectedFragment)


            }


            R.id.navigation_team -> {

                selectedFragment = TeamFragment()

                openFragment(selectedFragment)


            }
            R.id.navigation_employees -> {

                selectedFragment = EmployeeFragment()

                openFragment(selectedFragment)


            }

        }

        return true
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun objects(any: Any) {

        val intent = Intent(this, AddEmployeeActivity::class.java)
        intent.putExtra(AddEmployeeActivity.OBJECT, any as Team)
        startActivity(intent)
    }


}



