package com.example.anderson.employeesbalancing.features.employee

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.anderson.employeesbalancing.entity.Employee
import kotlinx.android.synthetic.main.item_list_employee.view.*


/**
 * Created by Anderson on 27/10/2018.
 */
class ViewHolderEmployee(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindView(employee: Employee) {

        val txtTeamName = itemView.employee_name
        val txtPlevel = itemView.plevel
        val txtBirthYear = itemView.birth_year
        val txtAdmissionYear = itemView.admission_year
        val txtLastProgressYear = itemView.last_progress_year


        txtTeamName.text = employee.nameEmployee
        txtPlevel.text = employee.plevel.toString()
        txtBirthYear.text = employee.birthYear.toString()
        txtAdmissionYear.text = employee.admissionYear.toString()
        txtLastProgressYear.text = employee.lastProgressionYear.toString()


    }


}