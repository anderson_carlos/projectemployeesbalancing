package com.example.anderson.employeesbalancing.features.employee


import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.repository.EmployeeRepository

/**
 * Created by Anderson on 26/10/2018.
 */
class EmployeePresenter(private val employeeView: EmployeeContract.EmployeeView) : EmployeeContract.EmployeePresenter {


    private var listEmployeesPromote = ArrayList<Employee>()
    private var employeeRepository: EmployeeRepository = EmployeeRepository()


    override fun getListEmployeePromote(qtdEmployeePromote: Int) {

        listEmployeesPromote = employeeRepository.getListEmployeePromote(qtdEmployeePromote)



        employeeView.callBackGetListEmployees(listEmployeesPromote)

        if (listEmployeesPromote.size > 0) {

            employeeView.displayUserMessage(true)

        } else {

            employeeView.displayUserMessage(false)

        }

    }


}