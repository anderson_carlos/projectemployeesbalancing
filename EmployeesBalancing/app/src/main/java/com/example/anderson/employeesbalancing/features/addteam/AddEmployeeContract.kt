package com.example.anderson.employeesbalancing.features.addteam

import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.Team

/**
 * Created by Anderson on 26/10/2018.
 */
interface AddEmployeeContract {


    interface AddEmployeeView {

        fun callBackGetListAddEmployee(employees: ArrayList<Employee>)


    }

    interface AddEmployeePresenter {

        fun getListAddEmployees()
        fun addEmployeesTeam (employeesList: ArrayList<Employee>, teamSelected : Team)

    }
}