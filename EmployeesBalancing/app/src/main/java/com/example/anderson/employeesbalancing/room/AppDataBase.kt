package com.example.anderson.employeesbalancing.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.EmployeeWithTeam
import com.example.anderson.employeesbalancing.entity.Team
import com.example.anderson.employeesbalancing.room.dao.EmployeeDao
import com.example.anderson.employeesbalancing.room.dao.EmployeeWithTeamDao
import com.example.anderson.employeesbalancing.room.dao.TeamDao

/**
 * Created by Anderson on 27/10/2018.
 */
@Database(entities = arrayOf(Employee::class,Team::class, EmployeeWithTeam::class), version = 1 ,exportSchema = false)
abstract class AppDataBase : RoomDatabase() {

    abstract fun employeeDao(): EmployeeDao
    abstract fun teamDao(): TeamDao
    abstract fun employeeWithTeamDao(): EmployeeWithTeamDao

}