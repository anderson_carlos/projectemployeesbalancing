package com.example.anderson.employeesbalancing.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * Created by Anderson on 26/10/2018.
 */
@Entity
@Parcelize
data class Employee (@PrimaryKey(autoGenerate = true) var idEmployee : Int,
                     var nameEmployee: String,
                     var plevel : Int,
                     var birthYear: Int,
                     var admissionYear: Int,
                     var lastProgressionYear: Int,
                     var punctuation: Int
                  ): Parcelable {
               // @Embedded
                //var team: Team? = null    var idTeam: Int
}