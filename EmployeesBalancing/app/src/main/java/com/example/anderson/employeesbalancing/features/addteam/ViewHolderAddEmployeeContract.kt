package com.example.anderson.employeesbalancing.features.addteam

import com.example.anderson.employeesbalancing.entity.Employee


/**
 * Created by Anderson on 26/10/2018.
 */
interface ViewHolderAddEmployeeContract {


    fun addObjectListEmployees(employe: Employee)

    fun removeObjectListEmployees(employe: Employee)


}