package com.example.anderson.employeesbalancing.features.addteam


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.Team
import kotlinx.android.synthetic.main.activity_add_employee.*

class AddEmployeeActivity : AppCompatActivity(), AddEmployeeContract.AddEmployeeView, ViewHolderAddEmployeeContract {

    private var mListEmployessAdd = ArrayList<Employee>()


    companion object {
        const val OBJECT = "OBJECT"

    }

    private lateinit var team: Team
    private lateinit var addEmployeePresenter: AddEmployeePresenter
    private lateinit var mViewHolderAddEmployeeContract: ViewHolderAddEmployeeContract


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_employee)


        mViewHolderAddEmployeeContract = this

        team = intent.getParcelableExtra(OBJECT)

        recyclerViewAddEmployee.layoutManager = LinearLayoutManager(this)

        addEmployeePresenter = AddEmployeePresenter(this)

        // Listar os funcionarios disponiveis
        addEmployeePresenter.getListAddEmployees()

        //Adicionar os funcionarios selecionados na equipe
        button_add_employees.setOnClickListener {

            addEmployeePresenter.addEmployeesTeam(mListEmployessAdd, team)

            finish()


        }

    }


    override fun callBackGetListAddEmployee(employees: ArrayList<Employee>) {
        recyclerViewAddEmployee.adapter = AdapterRecyclerViewListAddEmployee(employees, mViewHolderAddEmployeeContract)
    }


    override fun removeObjectListEmployees(employe: Employee) {
        mListEmployessAdd.remove(employe)
    }

    override fun addObjectListEmployees(employe: Employee) {
        mListEmployessAdd.add(employe)
    }


}
