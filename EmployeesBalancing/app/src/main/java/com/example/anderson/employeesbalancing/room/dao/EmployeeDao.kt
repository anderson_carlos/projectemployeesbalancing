package com.example.anderson.employeesbalancing.room.dao

import android.arch.persistence.room.*
import com.example.anderson.employeesbalancing.entity.Employee

/**
 * Created by Anderson on 27/10/2018.
 */
@Dao
interface EmployeeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEmployee( vararg employee: Employee)

    @Query("SELECT * FROM Employee as em WHERE em.idEmployee NOT IN ( SELECT e.idEmployee FROM Employee as e INNER JOIN EmployeeWithTeam as et ON e.idEmployee= et.employeeId) ")
    fun getEmployees() : List<Employee>

    @Query("SELECT * FROM Employee as e WHERE e.plevel < 5 ORDER BY e.punctuation DESC LIMIT :qtdEmployeePromote")
    fun getBiggerPointEmployees(qtdEmployeePromote : Int) : List<Employee>




    @Update
    fun updateEmployee(vararg employee: Employee)
}