package com.example.anderson.employeesbalancing.repository

import com.example.anderson.employeesbalancing.aplication.MyAplication
import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.EmployeeWithTeam
import com.example.anderson.employeesbalancing.entity.Team

/**
 * Created by Anderson on 31/10/2018.
 */
class AddEmployeeRepository {

    private var listAddEmployees = ArrayList<Employee>()

    fun getListAddEmployees(): ArrayList<Employee> {
        MyAplication.database?.employeeDao()?.getEmployees()?.let { listAddEmployees.addAll(it) }

        return listAddEmployees

    }


    fun addEmployeesTeam(employeesList: ArrayList<Employee>, teamSelected: Team) {

        var idTeam = 0
        var sumPleveisEmployees = 0



        if (teamSelected != null) {

            idTeam = teamSelected.id

        }
        // Pegar os funcionarios j� adicionados anteriormente
        MyAplication.database?.employeeWithTeamDao()?.getEmployeesLinkedTeam(teamSelected.id)?.let { employeesList.addAll(it) }


        if (employeesList.size > 0) {

            for (employee in employeesList) {

                var employeeWithTeam = EmployeeWithTeam(employee.idEmployee, idTeam)

                sumPleveisEmployees += employee.plevel


                LinkTeamEmployee(employeeWithTeam)

            }

            calculateExtraMaturityPoint(teamSelected, sumPleveisEmployees)

        }


    }


    private fun LinkTeamEmployee(employeeWithTeam: EmployeeWithTeam) {

        MyAplication.database?.employeeWithTeamDao()?.insertEmployeeWithTeam(employeeWithTeam)

    }

    private fun calculateExtraMaturityPoint(team: Team, sumPleveisEmployees: Int) {

        if (team.maturity < sumPleveisEmployees) {

            team.pointExtras = (sumPleveisEmployees - team.maturity)
            team.currentMaturity = sumPleveisEmployees
            MyAplication.database?.teamDao()?.updateTeam(team)

        }


    }


}