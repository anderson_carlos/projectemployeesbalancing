package com.example.anderson.employeesbalancing.features.loadCsv

import android.content.Context
import java.io.InputStream

/**
 * Created by Anderson on 28/10/2018.
 */
interface LoadCsvContract {

    interface LoadCsvView {


    }

    interface LoadCsvPresenter {


        fun LoadCsv(ctx: Context)


    }
}