package com.example.anderson.employeesbalancing.repository

import android.content.Context
import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.Util
import com.example.anderson.employeesbalancing.aplication.MyAplication
import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.Team
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.charset.Charset
import java.util.*

/**
 * Created by Anderson on 28/10/2018.
 */
class LoadCsvRepository {


    private lateinit var bufferedReader: BufferedReader
    private lateinit var team: Team
    private lateinit var employee: Employee


    fun LoadDataCsv(ctx: Context) {


        insertDataFileCsv(ctx.resources.openRawResource(R.raw.team), "team")

        insertDataFileCsv(ctx.resources.openRawResource(R.raw.employee), "employee")


    }

    private fun insertDataFileCsv(inputStream: InputStream, typeCsv: String) {


        bufferedReader = BufferedReader(
                InputStreamReader(inputStream,
                        Charset.forName("UTF-8")))

        try {
            var line: String? = ""

            while (line != null) {
                line = bufferedReader.readLine()

                var tokens = line?.split(";")

                if (typeCsv == "team") {

                    if (tokens != null) {

                        team = Team(0, tokens[0], tokens[1].toInt(), tokens[2].toInt(), tokens[3].toInt())
                        MyAplication.database?.teamDao()?.insertTeam(team)


                    }

                } else if (typeCsv == "employee") {

                    if (tokens != null) {

                        var returnCalculatePunctuation = calculatePunctuationEmployee(tokens[1].toInt(), tokens[2].toInt(), tokens[3].toInt(), tokens[4].toInt())

                        employee = Employee(0, tokens[0], tokens[1].toInt(), tokens[2].toInt(), tokens[3].toInt(), tokens[4].toInt(), returnCalculatePunctuation)

                        MyAplication.database?.employeeDao()?.insertEmployee(employee)


                    }

                }

            }
            bufferedReader.close()

        } catch (e: IOException) {

            e.printStackTrace()

        }

    }


    private fun calculatePunctuationEmployee(plvel: Int, birthYear: Int, admissionYear: Int, lastProgressionYear: Int): Int {
        var punctuation = 0
        var companyTime : Int
        var timeWithoutProgression : Int
        var age: Int


        var currentYear = Util.returnCorrentYear()

        // Time of admission
        companyTime = (currentYear - admissionYear)

        // Time without progression
        timeWithoutProgression = (currentYear - lastProgressionYear)

        // Time by age
        age = (currentYear - birthYear)


        if (companyTime >= 1) {

            punctuation += (companyTime * 2)

        }

        if (plvel == 4 && timeWithoutProgression >= 2) {
            punctuation += (companyTime * 3)

        } else if (timeWithoutProgression >= 1) {

            punctuation += (companyTime * 3)
        }

        // 1 ponto a cada 5 anos
        var i = 1
        while (i < age) {

            if ((i % 5) == 0) {

                punctuation += 1

            }
            i++
        }


        return punctuation

    }

}