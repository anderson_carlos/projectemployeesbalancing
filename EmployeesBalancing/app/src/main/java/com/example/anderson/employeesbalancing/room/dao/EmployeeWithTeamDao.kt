package com.example.anderson.employeesbalancing.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.EmployeeWithTeam
import com.example.anderson.employeesbalancing.entity.Team

/**
 * Created by Anderson on 01/11/2018.
 */

@Dao
interface EmployeeWithTeamDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEmployeeWithTeam( vararg employeeWithTeam: EmployeeWithTeam)

    @Query("SELECT * FROM Employee as e INNER JOIN EmployeeWithTeam as et ON e.idEmployee= et.employeeId WHERE et.teamId = :idTeam ")
    fun getEmployeesLinkedTeam(idTeam : Int) : List<Employee>

    @Query("SELECT * FROM Team as t INNER JOIN EmployeeWithTeam as et ON  t.id= et.teamId  INNER JOIN  Employee as e on e.idEmployee = et.employeeId  WHERE e.plevel < 5 ORDER BY punctuation DESC LIMIT :qtdEmployeePromote")
    fun getTeamsEmployeesPromote(qtdEmployeePromote : Int) : List<Team>

    @Query("SELECT * FROM Team as t INNER JOIN EmployeeWithTeam as et ON  t.id= et.teamId  INNER JOIN  Employee as e on e.idEmployee = et.employeeId  WHERE et.employeeId IN (:EmployeesIds) ")
    fun getTeamsEmployeesPromoteTeste(EmployeesIds : List<Int>) : List<Team>


}