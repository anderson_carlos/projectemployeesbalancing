package com.example.anderson.employeesbalancing.repository

import com.example.anderson.employeesbalancing.Util
import com.example.anderson.employeesbalancing.aplication.MyAplication
import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.Team

/**
 * Created by Anderson on 01/11/2018.
 */
class EmployeeRepository {


    private lateinit var listEmployee: ArrayList<Employee>

    private lateinit var listTeamsToUpdate: ArrayList<Team>

    private lateinit var listEmployeesLinkedTeam: ArrayList<Employee>
    private lateinit var listIdsEmployees: ArrayList<Int>

    fun getListEmployeePromote(qtdEmployeePromote: Int): ArrayList<Employee> {

        listEmployee = ArrayList()

        MyAplication.database?.employeeDao()?.getBiggerPointEmployees(qtdEmployeePromote)?.let { listEmployee.addAll(it) }

        if (listEmployee.size > 0) {

            updatePlevelEmployees()
        }

        return listEmployee

    }


    private fun updatePlevelEmployees() {
        listIdsEmployees = ArrayList()

        for (employee in listEmployee) {


            listIdsEmployees.add(employee.idEmployee)


            employee.plevel = (employee.plevel + 1)
            employee.lastProgressionYear = Util.returnCorrentYear()

            var returnReCalculateEmployeePoints = reCalculateEmployeePoints(employee.plevel, employee.birthYear, employee.admissionYear, employee.lastProgressionYear)

            employee.punctuation = returnReCalculateEmployeePoints

            MyAplication.database?.employeeDao()?.updateEmployee(employee)


        }

        reCalculateExtraMaturityPoint()


    }


    private fun reCalculateExtraMaturityPoint() {
        var sumPleveisEmployees = 0
        var lastTeamSelected = 0

        listTeamsToUpdate = ArrayList()

        MyAplication.database?.employeeWithTeamDao()?.getTeamsEmployeesPromoteTeste(listIdsEmployees)?.let { listTeamsToUpdate.addAll(it) }

        if (listTeamsToUpdate.size > 0) {

            for (team in listTeamsToUpdate) {


                if (lastTeamSelected != team.id) {


                    listEmployeesLinkedTeam = ArrayList()

                    MyAplication.database?.employeeWithTeamDao()?.getEmployeesLinkedTeam(team.id)?.let { listEmployeesLinkedTeam.addAll(it) }
                    lastTeamSelected = team.id

                    if (listEmployeesLinkedTeam.size > 0) {

                        for (employeeLikendTeam in listEmployeesLinkedTeam) {

                            sumPleveisEmployees += employeeLikendTeam.plevel


                        }

                        updateTeams(team, sumPleveisEmployees)

                    }

                }
            }

        }

    }

    private fun updateTeams(team: Team, sumPleveisEmployees: Int) {


        if (team.maturity < sumPleveisEmployees) {

            team.pointExtras = (sumPleveisEmployees - team.maturity)
            team.currentMaturity = sumPleveisEmployees

        }

        MyAplication.database?.teamDao()?.updateTeam(team)

    }


    private fun reCalculateEmployeePoints(plvel: Int, birthYear: Int, admissionYear: Int, lastProgressionYear: Int): Int {
        var punctuation = 0
        var companyTime = 0
        var timeWithoutProgression = 0
        var age = 0


        var currentYear = Util.returnCorrentYear()

        // Time of admission
        companyTime = (currentYear - admissionYear)

        // Time without progression
        timeWithoutProgression = (currentYear - lastProgressionYear)

        // Time by age
        age = (currentYear - birthYear)

        if (companyTime >= 1) {

            punctuation += (companyTime * 2)

        }

        if (plvel == 4 && timeWithoutProgression >= 2) {
            punctuation += (companyTime * 3)

        } else if (timeWithoutProgression >= 1) {

            punctuation += (companyTime * 3)
        }

        // 1 ponto a cada 5 anos
        var i = 1
        while (i < age) {

            if ((i % 5) == 0) {

                punctuation += 1

            }
            i++
        }

        return punctuation

    }


}