package com.example.anderson.employeesbalancing.room.dao

import android.arch.persistence.room.*
import com.example.anderson.employeesbalancing.entity.Team

/**
 * Created by Anderson on 27/10/2018.
 */
@Dao
interface TeamDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTeam( vararg team: Team)

    @Query("SELECT * FROM team")
    fun getTeams() : List<Team>

    @Query("SELECT * FROM team ORDER BY pointExtras  ASC LIMIT 1 ")
    fun teamLowestPointExtra() : Team

    @Query("SELECT * FROM team ORDER BY pointExtras   DESC LIMIT 1 ")
    fun teamBiggerPointExtra() : Team

    @Update
    fun updateTeam(vararg team: Team)
}