package com.example.anderson.employeesbalancing.features.team

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.anderson.employeesbalancing.entity.Team
import com.example.anderson.employeesbalancing.features.CallListener
import kotlinx.android.synthetic.main.item_list_team.view.*

/**
 * Created by Anderson on 27/10/2018.
 */
class ViewHolderTeam (itemView : View) : RecyclerView.ViewHolder(itemView){


    fun bindView(team: Team, mCallListener: CallListener) {

        val txtTeamName = itemView.team_name
        val txtPointExtra = itemView.point_extra
        val txtMinMatirity= itemView.min_maturity
        val txtCurrentMatirity = itemView.current_maturity

        txtTeamName.text = team.nameTeam
        txtPointExtra.text = team.pointExtras.toString()
        txtMinMatirity.text = team.maturity.toString()
        txtCurrentMatirity.text = team.currentMaturity.toString()

        itemView.button_add_employee.setOnClickListener {

            mCallListener.objects(team)

        }

    }


}