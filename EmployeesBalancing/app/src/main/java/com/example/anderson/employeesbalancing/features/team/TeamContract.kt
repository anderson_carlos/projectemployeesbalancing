package com.example.anderson.employeesbalancing.features.team

import com.example.anderson.employeesbalancing.entity.Team

/**
 * Created by Anderson on 26/10/2018.
 */
interface TeamContract {


    interface TeamView {

        fun callBackGetListTeams(teams: ArrayList<Team>)
        fun callBackResultBlance ( result: Int)

    }

    interface TeamPresenter {

        fun getListTeams()
        fun caulateResultBalance()

    }
}