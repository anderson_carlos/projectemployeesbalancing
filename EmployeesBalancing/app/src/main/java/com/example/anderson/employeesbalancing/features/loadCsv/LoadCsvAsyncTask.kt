package com.example.anderson.employeesbalancing.features.loadCsv

import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Handler
import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.features.loadCsv.LoadCsvContract
import com.example.anderson.employeesbalancing.repository.LoadCsvRepository

/**
 * Created by Anderson on 02/11/2018.
 */
class LoadCsvAsyncTask(private var ctx: Context, private val loadCsvPresenter: LoadCsvContract.LoadCsvPresenter) : AsyncTask<String, Void, Boolean>() {

    private val loadCsvRepository: LoadCsvRepository = LoadCsvRepository()

    private lateinit var progress: ProgressDialog

    override fun onPreExecute() {
        super.onPreExecute()

        progress = ProgressDialog(ctx)
        progress.setMessage(ctx.getString(R.string.msg_load_data))
        progress.setCancelable(false)
        progress.show()


    }


    override fun doInBackground(vararg params: String?): Boolean {

        loadCsvRepository.LoadDataCsv(ctx)

        return true
    }

    override fun onPostExecute(result: Boolean?) {
        super.onPostExecute(result)
        val handler = Handler()
        handler.postDelayed({ progress.dismiss() }, 6000)


    }


}