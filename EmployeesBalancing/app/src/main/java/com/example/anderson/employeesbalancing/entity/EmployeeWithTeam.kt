package com.example.anderson.employeesbalancing.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

/**
 * Created by Anderson on 01/11/2018.
 */

@Entity(tableName = "EmployeeWithTeam",
        primaryKeys = arrayOf("employeeId", "teamId"),
        foreignKeys =
        arrayOf(ForeignKey(
                entity = Team::class, parentColumns = arrayOf("id"),
                childColumns = arrayOf("teamId")),
                ForeignKey(entity = Employee::class,
                        parentColumns = arrayOf("idEmployee"),
                        childColumns = arrayOf("employeeId"))))
data class EmployeeWithTeam( var employeeId : Int,
var teamId :Int) {
}