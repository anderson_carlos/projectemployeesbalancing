package com.example.anderson.employeesbalancing.features.team


import com.example.anderson.employeesbalancing.entity.Team
import com.example.anderson.employeesbalancing.repository.TeamRepository


/**
 * Created by Anderson on 26/10/2018.
 */
class TeamPresenter(private val teamView: TeamContract.TeamView) : TeamContract.TeamPresenter {

    private lateinit var listTeams: ArrayList<Team>
    private var teamRepository: TeamRepository = TeamRepository()
    private var resultBalance: Int = 0

    override fun getListTeams() {

        listTeams = teamRepository.getListTeams()

        if (listTeams.size > 0) {

            teamView.callBackGetListTeams(listTeams)

        }
    }

    override fun caulateResultBalance() {

         resultBalance = teamRepository.caulateResultBalance()

        teamView.callBackResultBlance(resultBalance)


    }


}