package com.example.anderson.employeesbalancing.repository

import com.example.anderson.employeesbalancing.aplication.MyAplication
import com.example.anderson.employeesbalancing.entity.Team

/**
 * Created by Anderson on 28/10/2018.
 */
class TeamRepository {

    private lateinit var listTeams: ArrayList<Team>
    private lateinit var teamLowestPointExtra: Team
    private lateinit var teamBiggerPointExtra: Team
    private var resultBalance: Int = 0


    fun getListTeams(): ArrayList<Team> {
        listTeams = ArrayList()

        MyAplication.database?.teamDao()?.getTeams()?.let { listTeams.addAll(it) }

        return listTeams
    }

    fun caulateResultBalance(): Int {

        try {


            teamLowestPointExtra = MyAplication.database?.teamDao()?.teamLowestPointExtra()!!
            teamBiggerPointExtra = MyAplication.database?.teamDao()?.teamBiggerPointExtra()!!

            if (teamLowestPointExtra.pointExtras > 0 || teamBiggerPointExtra.pointExtras > 0) {

                if (teamBiggerPointExtra.pointExtras > teamLowestPointExtra.pointExtras) {
                    resultBalance = (teamBiggerPointExtra.pointExtras - teamLowestPointExtra.pointExtras)
                }

            }

            return resultBalance
        } catch (e: NullPointerException) {

            return 0

        }


    }


}