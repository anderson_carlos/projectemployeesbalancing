package com.example.anderson.employeesbalancing.features.employee

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.entity.Employee
import kotlinx.android.synthetic.main.fragment_employee.*


class EmployeeFragment : Fragment(), EmployeeContract.EmployeeView {

    private lateinit var employeePresenter: EmployeePresenter
    private lateinit var editQtdPromoteEmployee: EditText


    companion object {
        const val QTDEMPLOYEEPROMOTE = "QTDEMPLOYEEPROMOTE"

    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_employee, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        recyclerViewEmployee.layoutManager = LinearLayoutManager(activity)


        employeePresenter = EmployeePresenter(this)

        editQtdPromoteEmployee = qtd_promote_employee


        button_qtd_promote_employee.setOnClickListener {

            if (editQtdPromoteEmployee.text.toString() != "") {

                var qtdPromoteEmployee = qtd_promote_employee.text.toString().toInt()

                employeePresenter.getListEmployeePromote(qtdPromoteEmployee)
            }

        }


    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putString(QTDEMPLOYEEPROMOTE, editQtdPromoteEmployee.text.toString())

    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            editQtdPromoteEmployee.setText(savedInstanceState.getString(QTDEMPLOYEEPROMOTE))
        }
    }


    override fun callBackGetListEmployees(employees: ArrayList<Employee>) {

        recyclerViewEmployee.adapter = AdapterRecyclerViewListEmployee(employees)
    }


    override fun callBackGetListEmployeePromote(employees: ArrayList<Employee>) {
        recyclerViewEmployee.adapter = AdapterRecyclerViewListEmployee(employees)
    }


    override fun displayUserMessage(isSucesso: Boolean) {

        if (isSucesso) {
            Toast.makeText(activity, getString(R.string.msg_promote), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, getString(R.string.msg_no_promote), Toast.LENGTH_SHORT).show()
        }

    }


}
