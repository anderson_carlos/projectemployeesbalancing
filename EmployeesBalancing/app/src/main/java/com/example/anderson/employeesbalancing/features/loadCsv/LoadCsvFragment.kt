package com.example.anderson.employeesbalancing.features.loadCsv


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.anderson.employeesbalancing.R
import kotlinx.android.synthetic.main.fragment_load_csv.*


class LoadCsvFragment : Fragment(), LoadCsvContract.LoadCsvView {


    private lateinit var loadCsvPresenter: LoadCsvPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_load_csv, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        loadCsvPresenter = LoadCsvPresenter(this)


        load_data_csv.setOnClickListener {


            loadCsvPresenter.LoadCsv(activity)
        }

    }



}
