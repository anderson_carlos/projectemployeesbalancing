package com.example.anderson.employeesbalancing.features.employee

import com.example.anderson.employeesbalancing.entity.Employee

/**
 * Created by Anderson on 26/10/2018.
 */
interface EmployeeContract {

    interface EmployeeView {

        fun callBackGetListEmployees(employees: ArrayList<Employee>)
        fun callBackGetListEmployeePromote(employees: ArrayList<Employee>)
        fun displayUserMessage(isSucesso: Boolean)

    }

    interface EmployeePresenter {


        fun getListEmployeePromote(qtdEmployeePromote : Int)

    }
}