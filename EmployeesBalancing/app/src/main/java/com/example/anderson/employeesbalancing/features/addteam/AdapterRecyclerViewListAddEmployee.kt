package com.example.anderson.employeesbalancing.features.addteam

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.entity.Employee

/**
 * Created by Anderson on 27/10/2018.
 */
class AdapterRecyclerViewListAddEmployee(private var listAddEmployees: ArrayList<Employee>,
                                        private val mViewHolderAddEmployeeContract : ViewHolderAddEmployeeContract) : RecyclerView.Adapter<ViewHolderAddEmployee>() {




    override fun onBindViewHolder(holder: ViewHolderAddEmployee?, position: Int) {
        val employee = listAddEmployees[position]

        holder?.bindView(employee,mViewHolderAddEmployeeContract)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolderAddEmployee {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_add_employee, parent, false)
        return ViewHolderAddEmployee(view)
    }

    override fun getItemCount(): Int {
        return listAddEmployees.size
    }


}