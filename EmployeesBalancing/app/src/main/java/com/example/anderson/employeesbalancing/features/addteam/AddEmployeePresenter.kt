package com.example.anderson.employeesbalancing.features.addteam


import com.example.anderson.employeesbalancing.entity.Employee
import com.example.anderson.employeesbalancing.entity.Team
import com.example.anderson.employeesbalancing.repository.AddEmployeeRepository


/**
 * Created by Anderson on 26/10/2018.
 */
class AddEmployeePresenter(private val addTeamView: AddEmployeeContract.AddEmployeeView) : AddEmployeeContract.AddEmployeePresenter {


    private var listAddEmployees = ArrayList<Employee>()
    private var AddEmployeeRepository: AddEmployeeRepository = AddEmployeeRepository()

    override fun getListAddEmployees() {

        listAddEmployees = AddEmployeeRepository.getListAddEmployees()

        if (listAddEmployees.size > 0) {

            addTeamView.callBackGetListAddEmployee(listAddEmployees)
        }

    }

    override fun addEmployeesTeam(employeesList: ArrayList<Employee>, teamSelected: Team) {

        AddEmployeeRepository.addEmployeesTeam(employeesList, teamSelected)
    }
}