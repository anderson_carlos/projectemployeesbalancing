package com.example.anderson.employeesbalancing.features.loadCsv

import android.content.Context


/**
 * Created by Anderson on 28/10/2018.
 */
class LoadCsvPresenter(private val loadCsvView: LoadCsvContract.LoadCsvView) : LoadCsvContract.LoadCsvPresenter {


    lateinit var loadCsvAsyncTask: LoadCsvAsyncTask

    override fun LoadCsv(ctx: Context) {

        loadCsvAsyncTask = LoadCsvAsyncTask(ctx, this)
        loadCsvAsyncTask.execute()

    }

}