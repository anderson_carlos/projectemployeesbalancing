package com.example.anderson.employeesbalancing.features.team

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.entity.Team
import com.example.anderson.employeesbalancing.features.CallListener

/**
 * Created by Anderson on 27/10/2018.
 */
class AdapterRecyclerViewListTeam(private var listTeams: ArrayList<Team>, private  val mCallListener: CallListener)  : RecyclerView.Adapter<ViewHolderTeam>() {


    override fun onBindViewHolder(holder: ViewHolderTeam?, position: Int) {
        val team = listTeams[position]

        holder?.bindView(team,mCallListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolderTeam {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_team, parent, false)
        return ViewHolderTeam(view)
    }

    override fun getItemCount(): Int {
        return listTeams.size
    }
}