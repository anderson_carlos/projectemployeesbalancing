package com.example.anderson.employeesbalancing.aplication

import android.app.Application
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import com.example.anderson.employeesbalancing.room.AppDataBase

/**
 * Created by Anderson on 27/10/2018.
 */
class MyAplication : Application() {

    companion object {
        var database: AppDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()

        database = Room.databaseBuilder(
                this,
                AppDataBase::class.java,
                "DbEmployeesBalancing").allowMainThreadQueries().build()
    }
}