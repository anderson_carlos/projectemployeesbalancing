package com.example.anderson.employeesbalancing.features.employee

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.anderson.employeesbalancing.R
import com.example.anderson.employeesbalancing.entity.Employee

/**
 * Created by Anderson on 27/10/2018.
 */
class AdapterRecyclerViewListEmployee(private var listEmployees: ArrayList<Employee>) : RecyclerView.Adapter<ViewHolderEmployee>() {


    override fun onBindViewHolder(holder: ViewHolderEmployee?, position: Int) {
        val employee = listEmployees[position]

        holder?.bindView(employee)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolderEmployee {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_employee, parent, false)
        return ViewHolderEmployee(view)
    }

    override fun getItemCount(): Int {
        return listEmployees.size
    }
}