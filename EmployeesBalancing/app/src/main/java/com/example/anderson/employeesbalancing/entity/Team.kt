package com.example.anderson.employeesbalancing.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.Relation
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Anderson on 26/10/2018.
 */
@Entity
@Parcelize
data class Team (@PrimaryKey(autoGenerate = true) var id : Int,
                 var nameTeam : String,
                 var maturity : Int,
                 var pointExtras: Int,
                 var currentMaturity : Int
               ): Parcelable {

   // @Relation(parentColumn = "id", entityColumn = "id", entity = Employee::class)
    //var employessList: List<Employee>? = null


}